'use strict'

//Modelo estructural del usuario
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = Schema({
    name: String,
    surname: String,
    nickname: String,
    description: String,
    birthdate: String,
    email: String,
    password: String,
    role: String,
    image: String,
    enrollat: String,
});

module.exports = mongoose.model('User', userSchema);
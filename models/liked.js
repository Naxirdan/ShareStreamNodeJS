'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var likedSchema = Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    movieLiked: { type: Schema.ObjectId, ref: 'Movie' },
    serieLiked: { type: Schema.ObjectId, ref: 'Serie' },
    liekdAt: String
});

module.exports = mongoose.model('Liked', likedSchema);
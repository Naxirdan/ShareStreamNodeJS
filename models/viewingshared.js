'use strict'

//Hacer un array de usuarios asociados a un contenido, ya sea serie o pelicula.
//Ver la funcionalidad de compartir los tiempos de ejecucion del contenido en ambos seguimientos.
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var viewingSharedSchema = Schema({
    userOrder: { type: Schema.ObjectId, ref: 'User'},
    user: { type: Schema.ObjectId, ref: 'User'},
    viewing:{type: Schema.ObjectId, ref: 'Viewing'},
    timeView: String,
    sharedAt: String
});

module.exports = mongoose.model('ViewingShared', viewingSharedSchema);
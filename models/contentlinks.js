'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var linkSchema = Schema({
    contentMovie: {type: Schema.ObjectId, ref: 'Movie'},
    contentChapter: {type: Schema.ObjectId, ref: 'Chapter'},
    url: String,
    uploadAt: String,
    userUp: { type: Schema.ObjectId, ref: 'ContentLink'},
});

module.exports = mongoose.model('ContentLink', linkSchema);
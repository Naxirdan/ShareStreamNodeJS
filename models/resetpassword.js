'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var resetPasswordSchema = Schema({
    user: { type: Schema.ObjectId, ref: 'User'},
    status: String,
    resetAt: String
});

module.exports = mongoose.model('ResetPassword', resetPasswordSchema);
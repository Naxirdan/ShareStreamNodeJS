'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var chapterSchema = Schema({
    season: { type: Schema.ObjectId, ref: 'Season'},
    numChapter: Number,
    title: String,
    description: String,
    poster: String,
    duration: String,
    uploadat: String,
    URI: String
});

module.exports = mongoose.model('Chapter', chapterSchema);
'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var movieSchema = Schema({
    imdbID: String,
    title: String,
    description: String,
    genre: String,
    category: String,
    year: String,
    creator: String,
    duration: String,
    poster: String,
    URI: String,
    uploadat: String,
    userUp: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Movie', movieSchema);
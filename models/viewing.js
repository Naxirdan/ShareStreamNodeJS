'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var viewingSchema = Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    movieViewed: { type: Schema.ObjectId, ref: 'Movie' },
    season: { type: Schema.ObjectId, ref: 'Season' },
    chapter: { type: Schema.ObjectId, ref: 'Chapter' },
    timeViewed: String,
    viewingAt: String
});

module.exports = mongoose.model('Viewing', viewingSchema);
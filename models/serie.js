'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var serieSchema = Schema({
    title: String,
    description: String,
    genre: String,
    category: String,
    year: String,
    creator: String,
    duration: String,
    poster: String,
    uploadat: String,
    userUp: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Serie', serieSchema);
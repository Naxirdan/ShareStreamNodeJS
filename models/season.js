'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var seasonSchema = Schema({
    serie: { type: Schema.ObjectId, ref: 'Serie' },
    numSeason: Number,
    description: String,
    poster: String,
});

module.exports = mongoose.model('Season', seasonSchema);
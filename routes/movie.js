'use strict'

var express = require('express');
var MovieController =  require('../controllers/movie');
var api = express.Router();
var mdAuth = require('../middlewares/auth');

var multipart = require('connect-multiparty');
var mdUploadPoster = multipart({uploadDir: './uploads/movies/posters'});
var mdUploadVideo = multipart({uploadDir: './uploads/videos'});

api.post('/newmovie', mdAuth.ensureAuth, MovieController.newMovie);
api.get('/getmovie/:id', mdAuth.ensureAuth, MovieController.getMovie);
api.get('/getallmovies/:page?', mdAuth.ensureAuth, MovieController.getMovies);
api.put('/updatemovie/:id', mdAuth.ensureAuth, MovieController.updateMovie);
api.get('/moviesviewing/:page?', mdAuth.ensureAuth, MovieController.getMoviesViewing);
api.post('/movietitle/:page?', mdAuth.ensureAuth, MovieController.getMovieTitle);
api.delete('/deletemovie/:id', mdAuth.ensureAuth, MovieController.deleteMovie);
api.post('/uploadmovieposter/:id', [mdAuth.ensureAuth, mdUploadPoster], MovieController.uploadPosterMovie);
api.get('/getmovieposter/:id', [mdAuth.ensureAuth, mdUploadPoster], MovieController.getMoviePoster);
api.get('/get-movie-genre/:genre/:yearf/:yearl', mdAuth.ensureAuth, MovieController.filterMovieGenre);
api.get('/get-movie-year/:yearf/:yearl', mdAuth.ensureAuth, MovieController.filterMovieYear);
api.get('/search-movie/:search', mdAuth.ensureAuth, MovieController.filterSearch);
api.get('/get-movie-imdb/:imdbID', mdAuth.ensureAuth, MovieController.getMovieImdbID);

module.exports = api;
'use strict'

var express = require('express');
var ChapterController =  require('../controllers/chapter');
var api = express.Router();
var mdAuth = require('../middlewares/auth');

var multipart = require('connect-multiparty');

api.post('/newchapter/:id', mdAuth.ensureAuth, ChapterController.newChapter);
api.get('/getchapter/:id', mdAuth.ensureAuth, ChapterController.getChapter);
api.get('/getallchapters/:page?', mdAuth.ensureAuth, ChapterController.getChapters);


module.exports = api;
'use strict'

var express = require('express');
var SerieController =  require('../controllers/serie');
var api = express.Router();
var mdAuth = require('../middlewares/auth');

var multipart = require('connect-multiparty');
var mdUploadPoster = multipart({uploadDir: './uploads/series/posters'});
var mdUploadVideo = multipart({uploadDir: './uploads/videos'});

api.post('/newserie', mdAuth.ensureAuth, SerieController.newSerie);
api.get('/getserie/:id', mdAuth.ensureAuth, SerieController.getSerie);
api.get('/getallseries/:page?', mdAuth.ensureAuth, SerieController.getSeries);
api.put('/updateserie/:id', mdAuth.ensureAuth, SerieController.updateSerie);
api.get('/getviewingseries/:page?', mdAuth.ensureAuth, SerieController.getSeriesViewing);
api.post('/getserietitle/:page?', mdAuth.ensureAuth, SerieController.getSerieTitle);
api.delete('/deleteserie/:id', mdAuth.ensureAuth, SerieController.deleteSerie);
api.post('/uploadposterserie/:id', [mdAuth.ensureAuth, mdUploadPoster], SerieController.uploadPosterSerie);
api.get('/getserieposter/:id', mdAuth.ensureAuth, SerieController.getSeriePoster);
api.get('/get-serie-genre/:genre', mdAuth.ensureAuth, SerieController.filterSerieGenre);
api.get('/get-serie-year/:year', mdAuth.ensureAuth, SerieController.filterSerieYear);
api.get('/search-series/:search', mdAuth.ensureAuth, SerieController.filterSearch);

module.exports = api;
'use strict'

var express = require('express');
var SeasonController =  require('../controllers/season');
var api = express.Router();
var mdAuth = require('../middlewares/auth');

var multipart = require('connect-multiparty');

api.post('/newseason/:id', mdAuth.ensureAuth, SeasonController.newSeason);
api.get('/getseason/:id', mdAuth.ensureAuth, SeasonController.getSeason);
api.get('/getallseasons/:page?', mdAuth.ensureAuth, SeasonController.getSeasons);


module.exports = api;
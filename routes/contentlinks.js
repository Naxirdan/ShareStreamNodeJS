'use strict'

var express = require('express');
var ContentLinkController =  require('../controllers/contentlink');
var api = express.Router();
var mdAuth = require('../middlewares/auth');

var multipart = require('connect-multiparty');
var mdUploadVideo = multipart({uploadDir: './uploads/videos'});

api.post('/up-video/:tipo/:id', [mdAuth.ensureAuth, mdUploadVideo], ContentLinkController.uploadVideo);
api.get('/get-links/:idContent', mdAuth.ensureAuth, ContentLinkController.getLinks);
api.get('/get-content/:videoFile', mdAuth.ensureAuth, ContentLinkController.getVideoFile);

module.exports = api;
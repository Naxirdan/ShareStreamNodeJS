'use strict'

var express = require('express');

var UserController = require('../controllers/user');

var mdAuth = require('../middlewares/auth');

var api = express.Router();

var multipart = require('connect-multiparty');
var mdUpload = multipart({uploadDir: './uploads/users'});

api.get('/pruebas', UserController.pruebas);//Ruta de pruebas
api.post('/register', UserController.RegUser);//Ruta de inserción de datos de usuario.
api.post('/login', UserController.loginUser);//Ruta de Login
api.get('/user/:id', mdAuth.ensureAuth, UserController.getUser);//Ruta para hacer un getter al usuario
api.get('/allusers/:page?', mdAuth.ensureAuth, UserController.getUsers);//Ruta para mostrar todos los usuarios
api.put('/update-user/:id', mdAuth.ensureAuth, UserController.updateUser);//Actualizamos los datos de un usuario
api.post('/upload-image-user/:id', [mdAuth.ensureAuth, mdUpload], UserController.uploadImage);//Actualizacion de la imagen de perfil
api.get('/profile-image/:imageFile', mdAuth.ensureAuth, UserController.getImageFile);
api.get('/counters/:id?', mdAuth.ensureAuth, UserController.getCounters);
api.post('/send-reset', UserController.sendResetPassword);
api.get('/check-reset/:id', UserController.resStatus);
api.post('/update-reset/:id', UserController.resetPassword);
api.delete('/delete-user/:id', mdAuth.ensureAuth, UserController.deleteUser);

module.exports = api;
'use strict'

var express = require('express');
var ViewingController = require('../controllers/viewing');
var mdAuth = require('../middlewares/auth');
var api = express.Router();

api.post('/viewing/:type/:id', mdAuth.ensureAuth, ViewingController.viewingContent);
api.get('/getviewing/:type/:id?', mdAuth.ensureAuth, ViewingController.getViewing);
api.get('/getallviewing/:type/:id?/:page?', mdAuth.ensureAuth, ViewingController.getAllViewing);
api.delete('/delete-view/:id?', mdAuth.ensureAuth, ViewingController.deleteViewing);
api.get('/get-n-views/:type/:n', mdAuth.ensureAuth, ViewingController.getNview);

module.exports = api;
'use strict'

var express = require('express');
var LikedController = require('../controllers/liked');
var mdAuth = require('../middlewares/auth');
var api = express.Router();

api.post('/liked/:type/:id', mdAuth.ensureAuth, LikedController.likedContent);
api.get('/getlikeds/:id?', mdAuth.ensureAuth, LikedController.getLikeds);
api.get('/get-n-likes/:type/:n', mdAuth.ensureAuth, LikedController.getNliked);
api.delete('/dislikeds/:id', mdAuth.ensureAuth, LikedController.dislike);

module.exports = api;
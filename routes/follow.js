'use strict'

var express = require('express');
var FollowController = require('../controllers/follow');
var api = express.Router();

var mdAuth = require('../middlewares/auth');

api.post('/follow', mdAuth.ensureAuth, FollowController.saveFollow);
api.delete('/unfollow/:id', mdAuth.ensureAuth, FollowController.deleteFollow);
api.get('/get-following/:id?/:page?', mdAuth.ensureAuth, FollowController.getFollowingUsers);
api.get('/followers/:id?/:page', mdAuth.ensureAuth, FollowController.getFollowers);
api.get('/all-follow/:followed?', mdAuth.ensureAuth, FollowController.getAllFollow);

module.exports = api;
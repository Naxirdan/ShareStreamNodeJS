'use strict'

var express = require('express');
var ViewingSharedController = require('../controllers/viewingshared');
var mdAuth = require('../middlewares/auth');
var api = express.Router();

api.post('/viewingshare/:id/:userId', mdAuth.ensureAuth, ViewingSharedController.viewingShared);

module.exports = api;
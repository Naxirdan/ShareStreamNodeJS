'use strict'

//  Declaramos todos los paquetes y framework necesarios.
var express = require('express');
var bodyParser = require('body-parser');

//  inicializamos y cargamos la variable app con el framework express.
var app = express()

//  Cargar rutas
var UserRoutes = require('./routes/user');
var FollowRoutes = require('./routes/follow');
var MoviesRoutes = require('./routes/movie');
var SeriesRoutes = require('./routes/serie');
var ChapterRoutes = require('./routes/chapter');
var SeasonRoutes = require('./routes/season');
var LikedRoutes = require('./routes/liked');
var ViewingRoutes = require('./routes/viewing');
var ViewinSharedgRoutes = require('./routes/viewingshared');
var ContentLinkRoutes = require('./routes/contentlinks');

//  Middlewares -- Métodos que se cargan o ejecutan antes de la llegada a un controlador por parte de una petición.
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//  Cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE, OPEN');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});

//  rutas
app.use('/api', UserRoutes);
app.use('/api', FollowRoutes);
app.use('/api', MoviesRoutes);
app.use('/api', SeriesRoutes);
app.use('/api', ChapterRoutes);
app.use('/api', SeasonRoutes);
app.use('/api', LikedRoutes);
app.use('/api', ViewingRoutes);
app.use('/api', ViewinSharedgRoutes);
app.use('/api', ContentLinkRoutes);


//  Exportación
module.exports = app;
'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 9000;

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/sharestream', {useMongoClient:true})
        .then(() => {
            console.log("La conexion ha sido un exito absoluto.");
            //  Creacion del servidor
            app.listen(port, ()=>{
                console.log('El servidor se ha creado correctamente.');
            });
        })
        .catch(err => {
            console.log(err);
        })
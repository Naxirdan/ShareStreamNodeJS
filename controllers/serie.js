'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Serie = require('../models/serie');
var Season = require('../models/season');
var Chapter = require('../models/chapter');
var User = require('../models/user');
var Viewing = require('../models/viewing');
function newSerie(req, res){
    var params = req.body;

    if(!params.title || !params.description) return res.status(200).send({
        message: 'No has cumplimentado los campos obligatorios'
    });

    var serie = new Serie();

    serie.title = params.title;
    serie.description = params.description;
    serie.genre = params.genre;
    serie.category = params.category;
    serie.year = params.year;
    serie.creator = params.creators;
    serie.duration = params.duration;
    serie.poster = params.file;
    serie.uploadat = moment().unix();
    serie.userUp = req.user.sub;

    serie.save((err, serieStored) =>{
        if(err) res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!serieStored) res.status(404).send({
            message: 'Error al guardar la pelicula en la base de datos'
        });

        return res.status(200).send({
           serie: serieStored
        });
    });

}

//GETTERS  GETTERS  GETTERS  GETTERS

function getSerie(req, res){
    var serieId = req.params.id;

    Serie.findById(serieId, (err, serie) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!serie) return res.status(404).send({
            message: "Serie inexistente"
        });

        return res.status(200).send({
            message: serie
        });

        /*followThisserie(req.serie.sub, serieId).then((value) => { 
            serie.password = undefined;

            return res.status(200).send({
                serie, 
                following: value.following, 
                followed: value.followed
            });
        });*/
    });
}

function getSeries(req, res){
    var identitySerie =  req.params.id;

    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Serie.find().sort('_id').paginate(page, itemsPerPage, (err, series, total) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!series) return res.status(404).send({
            message: "No hay series disponibles"
        });

        return res.status(200).send({
            message: series
        });

        /*followSeriesId(identitySerie).then((value) => {
            return res.status(200).send({
                series,
                following: value.followingSeries,
                followed: value.followedSeries,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        })
        .catch(err =>{
            console.log(err);
        });*/
    });
}

//Actualizacion de datos del usuario
function updateSerie(req, res){

    var serieId = req.params.id;
    var update = req.body;

    //impedimos que el serie nos traiga contraseña
    delete update.password;

    Serie.findByIdAndUpdate(serieId, update, {new:true}, (err, serieUpdate) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });

        if(!serieUpdate) return res.status(404).send({
            message: 'No se ha encontrado usuario que actualizar'
        });

        return res.status(200).send({
            serie: serieUpdate
        });
    });
}


function getSeriesViewing(req, res){
    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }
    var itemsPerPage = 10;

    Viewing.find({user: req.user.sub, movieViewed: null}).populate('season').exec((err, viewing) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });
        var viewingClean = [];

        viewing.forEach((view)=>{
            viewingClean.push(view.season);
        });
        
        console.log(viewingClean);

        Season.find({_id: {$in: viewingClean}}).sort("viewedAt").populate("serie").paginate(page, itemsPerPage,(err, series, total)=>{
            console.log(series);
            if(err) return res.status(500).send({
                message: 'Error en la peticion de series'
            });

            if(!series) return res.status(400).send({
                message: 'No se han generado series vistas aun'
             });

             return res.status(200).send({
                series,
                pages: Math.ceil(total/itemsPerPage),
                page,
                total_items: total
             });
        });

    });
}

function getSerieTitle(req, res){
    var params = req.body;
    var page = 1;
    var itemsPerPage = 10;

    if(req.params.page){
        page = req.params.page; 
    }

    if(!params.title) return res.status(500).send({
        message: 'Barra de navegacion vacia'
    });

    Serie.find({title: params.title}).populate("serie").paginate(page, itemsPerPage,(err,series, total)=>{
        if(err) return res.status(500).send({
            message: 'Error al hacer la peticion al servidor'
        });

        if(!series) return res.status(404).send({
            message: 'No se han podido generar series que coincidan'
        });

        return res.status(200).send({
            series,
            total,
            pages: Math.ceil(total/itemsPerPage),
            page
        });
    });
}

function deleteSerie(req, res){
    var serieId = req.params.id;

    Serie.findById(serieId, (err, serie)=>{
        if(serie.userUp == req.user.sub){
            if(err) return res.status(500).send({
                message: 'La peticion al servidor ha sido incorrecta'
            });
    
            if(!serie) return res.status(404).send({
                message: 'Esta serie no se encuentra disponible en estos momentos'
            });

            Serie.findByIdAndRemove(serieId, (err, serieRemoved)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion serie'
                });

                if(!serieRemoved) return res.status(404).send({
                    message: 'No se ha encontrado referencia al serieed'
                });

                return res.status(200).send({
                    serieRemoved
                });
            });
        }else{
            return res.status(200).send({
                message: 'Este usuario no puede borrar este contenido'
            });
        }
    });
}

function uploadPosterSerie(req, res){
    var serieId = req.params.id;

    console.log(req.files);

    if(req.files){
        var filePath = req.files.image.path; //Recojo el path de imagen
        var fileSplit = filePath.split('\/'); //Corto la ruta en una Array
        var fileName = fileSplit[3]; //Del Array anterior solo me quedo con el nombre del fichero
        var extSplit = fileName.split('\.'); //Cortamos el nombre del fichero en un Array
        var fileExt = extSplit[1]; //N os quedamos con la parte de la extension para verificar el tipo de fichero

        if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
            //Actualizamos poster
            Serie.findByIdAndUpdate(serieId, {poster: fileName}, {new:true}, (err, serieUpdate)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion'
                });
        
                if(!serieUpdate) return res.status(404).send({
                    message: 'No se ha encontrado usuario que actualizar'
                });
        
                return res.status(200).send({
                    serie: serieUpdate
                });
            });
        }else{
            return removeFilesUpload(res, filePath, 'Extension incorrecta');
        }

    }else{
        return res.status(200).send({
            message: 'No se han subido archivos'
        });
    }
}

function getSeriePoster(req, res){
    var imageFile = req.params.id;
    var pathFile = './uploads/series/posters/'+imageFile;

    fs.exists(pathFile, (exists) => {
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

function removeFilesUpload(res, filePath, errorMessage){
    fs.unlink(filePath, (err) => {
        return res.status(200).send({
            message: errorMessage
        });
    });
}


function filterSerieGenre(req, res){
    var filterBy= req.params.genre;

    Serie.find({genre: filterBy}, (err, serie)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!serie) return res.status(404).send({
            message: 'No se han encontrado peliculas con este género'
        });

        return res.status(200).send({
            serie
        });
    });
}

function filterSerieYear(req, res){
    var filterBy= req.params.year;

    Serie.find({year: filterBy}, (err, serie)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!serie) return res.status(404).send({
            message: 'No se han encontrado peliculas con este año'
        });

        return res.status(200).send({
            serie
        });
    });
}

function filterSearch(req, res){
    var search = req.params.search;
    Serie.find({title: {$regex: search, $options: 'i'}}, (err,series)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });
 
        if(!series) return res.status(404).send({
            message: 'No hemos encontrado coincidencias en el servidor'
        });
 
        return res.status(200).send({
            series
        });
 
    });
 }

module.exports = {
    newSerie,
    getSerie,
    getSeries,
    updateSerie,
    getSeriesViewing,
    getSerieTitle,
    deleteSerie,
    uploadPosterSerie,
    getSeriePoster,
    filterSerieGenre,
    filterSerieYear,
    filterSearch
}
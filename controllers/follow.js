'use strict'

//var path = require('path');
//var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

var User = require('../models/user');
var Follow = require('../models/follow');

function saveFollow(req, res){
    var params = req.body;
    var follow = new Follow();

    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) =>{
        if(err) return res.status(200).send({
            message: 'Error al guardar el seguimiento.'
        });

        if(!followStored) return res.status(404).send({
            message: 'El seguimiento no se ha guardado.'
        });

        return res.status(200).send({
            follow: followStored
        });
    });
}

function deleteFollow(req, res){
    var userId = req.user.sub;
    var followedId = req.params.id;

    Follow.find({user: userId, followed: followedId}).remove(err=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });

        return res.status(200).send({
            message: 'Has dejado de seguir al usuario:'+followedId
        });
    });
}

function getFollowingUsers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId = req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }else{
        page = req.params.id;
    }

    var itemsPerPage = 10;

    Follow.find({user: userId}).populate({path: 'followed'}).paginate(page, itemsPerPage, (err, follows, total) => {
        if(err) return res.status(500).send({
            message: 'Error en el servidor'
        });

        if(!follows) return res.status(404).send({
            message: 'No hay usuarios seguidos todavia'
        });

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            follows
        });

    });
}

function getFollowers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId = req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }else{
        page = req.params.id;
    }

    var itemsPerPage = 10;

    Follow.find({followed: userId}).populate('user').paginate(page, itemsPerPage, (err, follows, total) => {
        if(err) return res.status(500).send({
            message: 'Error en el servidor'
        });

        if(!follows) return res.status(404).send({
            message: 'No hay usuarios seguidos todavia'
        });

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            follows
        });

    });
}

function getAllFollow(req, res){
    var userId = req.user.sub;

    var find = Follow.find({user: userId});
    var swap = 'followed'

    if(req.params.followed){
        find = Follow.find({followed: userId});
        var swap = 'user'
    }

    find.populate(swap).exec((err, follows)=>{
        if(err) return res.status(500).send({
            message: 'Error en el servidor'
        });

        if(!follows) return res.status(404).send({
            message: 'No hay usuarios seguidos todavia'
        });

        return res.status(200).send({
            follows
        });
    });
}

module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowers,
    getAllFollow
}
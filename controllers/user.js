'use strict'

//Diferentes acciones a hacer con el operador del usuario

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Follow = require('../models/follow');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var Viewing = require('../models/viewing');
var ViewingShared = require('../models/viewingshared');
var Liked = require('../models/liked');
var moment = require('moment');
var ResetPass = require('../models/resetpassword');

function pruebas(req, res){
    res.status(200).send({
        message: 'Funciono wey!'
    });
}

//Registro del usuario
function RegUser(req, res){
    var params = req.body;
    var user = new User();

    if(params.name && params.surname && params.email && params.nickname && params.password){
        user.name = params.name;
        user.surname = params.surname;
        user.nickname = params.nickname;
        user.description = params.description;
        user.email = params.email;
        user.birthdate = params.birthdate;
        user.image = 'profileNull.png';
        user.role = 'USER_ROLE';
        user.enrollat = moment().unix();

        //Comprobar que no exista ya el usuario en la base de datos.
        User.find({ $or: [
                        {email: user.email.toLowerCase()},
                        {nickname: user.nickname.toLowerCase()}
                    ]}).exec((err,users) => {
                        if(err) return res.status(500).send({
                            message: 'Error en la ejecucion'
                        });

                        if(users && users.length >= 1){
                            return res.status(200).send({
                                message: 'Este usuario ya existe'
                            });
                        }else{
                            //Continuamos con la encriptacion de la contraseña
                            bcrypt.hash(params.password, null, null, (err, hash) => {
                                user.password = hash;
                                user.save((err, userStored) => {
                                    if(err) return res.status(500).send({
                                        message: 'Error al guardar los datos de usuario.'
                                    });

                                    if(userStored){
                                        res.status(200).send({
                                            user: userStored
                                        });
                                    }else{
                                        res.status(404).send({
                                            message:'Usuario no registrado.'
                                        });
                                    }

                                });
                            });
                            user.password = params.password;
                        }
                    });

    }else{
        res.status(200).send({
            message: 'Envia todos los campos necesarios.'
        });
    }
}

//Validar datos de login del usuario
function loginUser(req, res){
    var params = req.body;
    var user = new User();

    var account = params.emailNick;
    var password = params.password;

    User.findOne({ $or: [
        {email: account},
        {nickname: account}
        ]}, (err, user) => {
        if(err) return res.status(500).send({message:"No se ha podido realizar la conexion"});

        if(user){
            console.log(user);
            bcrypt.compare(password, user.password, (err, check) => {
                if(check){
                    if(params.gettoken){
                        //Devolver y generar tokens
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });
                    }else{
                        user.email=undefined;
                        user.password=undefined;

                        return res.status(200).send({user});
                    }
                }else{
                    return res.status(404).send({message:"El usuario no ha podido iniciar sesion"});
                }
            });
        }else{
            return res.status(404).send({message:"El usuario no ha sido encontrado"});
        }
    });
}

//Sacar todos los datos de un usuario:
function getUser(req, res){
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!user) return res.status(404).send({
            message: "Usuario inexistente"
        });

        followThisuser(req.user.sub, userId).then((value) => { 
            user.password = undefined;

            return res.status(200).send({
                user, 
                following: value.following, 
                followed: value.followed
            });
        });
    });
}

async function followThisuser(identityUser, userId){
    var following = await Follow.findOne({'user': identityUser, 'followed': userId}, (err, follow)=>{
        if(err) return handleError(err);
        return follow;
    });

    var followed = await Follow.findOne({'user': userId, 'followed': identityUser}, (err, follow)=>{
        if(err) return handleError(err);
        return follow;
    });

    return {
        following: following,
        followed: followed
    };
}

//Paginacion de usuarios 
function getUsers(req, res){
    var identityUser =  req.user.sub;

    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!users) return res.status(404).send({
            message: "No hay usuarios disponibles"
        });

        followUsersId(identityUser).then((value) => {
            return res.status(200).send({
                users,
                following: value.followingUsers,
                followed: value.followedUsers,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        })
        .catch(err =>{
            console.log(err);
        });
    });
}

async function followUsersId(userId){
    
    var following = await Follow.find({'user': userId}, (err, follows)=>{
        if(err) return res.status(500).send({
            message:'error en la peticion'
        });
        var followingClean = [];

        follows.forEach((follow) => {
        followingClean.push(follow.followed);
    });
        return followingClean;
    }).select({'_id':0, '__v':0, 'user':0});
    
    var followed = await Follow.find({'followed': userId}, (err, follows)=>{
        if(err) return res.status(500).send({
            message:'error en la peticion'
        });
        var followedClean = [];
        follows.user=undefined;
        follows.forEach((follow) => {
        followedClean.push(follow.user);
    });
        return followedClean;
    }).select({'_id':0, '__v':0, 'followed':0});

    //Procesar following IDs

    //Procesar followed IDs

    return  {
        followingUsers: following,
        followedUsers: followed
    }
}

//Actualizacion de datos del usuario
function updateUser(req, res){

    var userId = req.params.id;
    var update = req.body;

    //impedimos que el user nos traiga contraseña
    delete update.password;

    User.findByIdAndUpdate(userId, update, {new:true}, (err, userUpdate) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });

        if(!userUpdate) return res.status(404).send({
            message: 'No se ha encontrado usuario que actualizar'
        });

        return res.status(200).send({
            user: userUpdate
        });
    });
}

//Subir avatar de usuario
function uploadImage(req, res){
    var userId = req.params.id;

    if(req.files){
        var filePath = req.files.image.path; //Recojo el path de imagen
        var fileSplit = filePath.split('\/'); //Corto la ruta en una Array
        var fileName = fileSplit[2]; //Del Array anterior solo me quedo con el nombre del fichero
        var extSplit = fileName.split('\.'); //Cortamos el nombre del fichero en un Array
        var fileExt = extSplit[1]; //N os quedamos con la parte de la extension para verificar el tipo de fichero

        if(userId != req.user.sub){
           return removeFilesUpload(res, filePath, 'No tienes permisos para actualizar este usuario');
        }

        if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
            //Actualizamos avatar
            User.findByIdAndUpdate(userId, {image: fileName}, {new:true}, (err, userUpdate)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion'
                });
        
                if(!userUpdate) return res.status(404).send({
                    message: 'No se ha encontrado usuario que actualizar'
                });
        
                return res.status(200).send({
                    user: userUpdate
                });
            });
        }else{
            return removeFilesUpload(res, filePath, 'Extension incorrecta');
        }

    }else{
        return res.status(200).send({
            message: 'No se han subido archivos'
        });
    }
}

function getImageFile(req, res){
    var imageFile = req.params.imageFile;
    var pathFile = './uploads/users/'+imageFile;

    fs.exists(pathFile, (exists) => {
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

function removeFilesUpload(res, filePath, errorMessage){
    fs.unlink(filePath, (err) => {
        return res.status(200).send({
            message: errorMessage
        });
    });
}

function getCounters(req, res){
    var userId = req.user.sub;
    if(req.params.id){
        userId = req.params.id;
    }
    getCountFollow(req.params.id).then((value)=>{
        return res.status(200).send({
            value
        });
    });
    
}

async function getCountFollow(userId){
    var following = await Follow.count({'user': userId},(err, count)=>{
        if(err) return handleError(err);
        console.log(count);
        return count;
    });

    var followed = await Follow.count({'followed': userId}, (err, count)=>{
        if(err) return handleError(err);
        console.log(count);
        return count;
    });
console.log(following);
    return {
        following: following,
        followed: followed
    };
}

function sendResetPassword(req, res){
    var params = req.body;

    if(!params.emailNick) return res.status(404).send({
        message: 'Debes rellenar el campo emailNick'
    });

    User.findOne({$or:[
            {email: params.emailNick},
            {nickname: params.emailNick}
        ]}, (err, userRes) => {
            
            if(err) return res.status(500).send ({
                message: 'Error en la peticion del servidor'
            });

            if(!userRes) return res.status(404).send({
                message: 'Usuario no encontrado, no es posible enviar la peticion de reseteo'
            });

            var resPass = new ResetPass();

            resPass.user = userRes;
            resPass.status = 'wait';
            resPass.resetAt = moment().unix();
            resPass.save((err, resStored) =>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion del servidor'
                });

                if(!resStored) return res.status(404).send({
                    message: 'No se ha podido ejecutar su peticion, intentalo más tarde'
                });

                return res.status(200).send({
                    resStored,
                });
            });
    });
}

function resStatus(req, res){
    var resetId = req.params.id;

    ResetPass.findById(resetId).populate({path: 'user'}).exec((err, resetObj) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion del servidor'
        });

        if(!resetObj) return res.status(404).send({
            message: 'No se ha encontrado peticion de reset'
        });

        if(resetObj.status === 'wait'){
            return res.status(200).send({
                resetObj
            });
        } else if(resetObj.status === 'used'){
            return res.status(200).send({
                message: 'Esta url ya ha sido utilizada'
            });
        }
    });
}

function resetPassword(req, res){
    var params = req.body;
    var userId = req.params.id;

    if(!params.password || !params. rePassword){
        res.status(200).send({
            message: 'No se han rellenado todos los campos'
        });
    }

    bcrypt.hash(params.password, null, null, (err, hash) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!hash) return res.status(404).send({
            message: 'No es posible realizar la encripacion de la clave'
        }); 

        User.findByIdAndUpdate(userId, {password: hash}, {new:true}, (err, userUpdate) =>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!userUpdate) return res.status(404).send({
            message: 'No es posible actualizar ningun usuario'
        }); 
        
        ResetPass.findOneAndUpdate({user: userId}, {'status': 'used'}, (err,resetUpdate)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion al servidor'
                });
        
                if(!resetUpdate) return res.status(404).send({
                    message: 'No es posible actualizar ningun resetPass'
                }); 

                return res.status(200).send({
                    resetUpdate,
                    userUpdate
                })
        });

        });
    });
}

function deleteUser(req, res){
    var userId = req.params.id;

    console.log(userId);

    User.findById(userId).remove().exec((err,resp) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!resp) return res.status(404).send({
            message: 'Este usuario no se puede borrar'
        });

        return res.status(200).send({
            resp
        });
    });
}

module.exports = {
    pruebas,
    RegUser,
    loginUser,
    getUser,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    getCounters,
    sendResetPassword,
    resStatus,
    resetPassword,
    deleteUser
}
'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Follow = require('../models/follow');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var Viewing = require('../models/viewing');
var Movie = require('../models/movie');
var ViewingShared = require('../models/viewingshared');
var Serie = require('../models/serie');
var Season = require('../models/season');
var Liked = require('../models/liked');
var moment = require('moment');

function likedContent(req, res){
    var typeCont = req.params.type;
    var contentId = req.params.id;
    var userId = req.user.sub;

    if(!typeCont || !contentId) {
        return res.status(404).send({message: 'Esta categoria no existe en la plataforma'});
    }

    var liked = new Liked();

    if(typeCont == 'movie'){

        liked.user = req.user.sub;
        liked.movieLiked = contentId;
        liked.likedAt = moment().unix();

        Liked.find({movieLiked: contentId, user: userId}, (err, compLiked)=>{
            if(err) return res.status(500).send({
               message: 'Error en la peticion al server'
            });
            if(compLiked.length!==0){
                return res.status(200).send({
                    message: 'Ya estás viendo esta película'
                });
            }else{
    
                liked.save((err, likedTimeStored) =>{
                    if(err) res.status(500).send({
                        message: 'Error en la peticion al servidor'
                    });
            
                    if(!likedTimeStored) res.status(404).send({
                        message: 'Error al guardar el tiempo de la pelicula en la base de datos'
                    });
            
                    return res.status(200).send({
                    movie: likedTimeStored
                    });
                });
            }
        });

    }else if(typeCont == 'serie'){

        liked.user = req.user.sub;
        liked.serieLiked = contentId;
        liked.likedAt = moment().unix();
    
        liked.save((err, likedTimeStored) =>{
            if(err) res.status(500).send({
                message: 'Error en la peticion al servidor'
            });
    
            if(!likedTimeStored) res.status(404).send({
                message: 'Error al guardar el tiempo del capitulo en la base de datos'
            });
    
            return res.status(200).send({
               movie: likedTimeStored
            });
        });
    }else{
        return res.status(404).send({
            message:'Esta categoria de contenido no existe'
        });
    }
}

function getLikeds(req, res){
    var userId = req.params.id;

    if(!userId){
        userId = req.user.sub;
    }

    Liked.find({user: userId}).populate('movieLiked').sort('likedAt').exec((err, likeds)=>{
        if(err) return res.status(500).send({
            message: 'Error al enviar la peticion'
        });

        if(!likeds || likeds=='') return res.status(200).send({
            message: 'No ha dado ningun me gusta aun'
        });

        return res.status(200).send({
            likeds
        });
    });

}

/*function dislike(req, res){
    dislikeId = req.params.id;

    if(!dislikeId) return res.status(500).send({
                        message: 'Error en la peticion al servidor'
                    });

        Liked.findById(dislikeId, (err, liked)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion'
            });

            if(!liked) res.status(404).send({
                message: 'Este contenido no se encuentra disponible para dar dislike'
            });

            if(liked.user == req.user.sub){
                Liked.findByIdAndRemove(dislikeId, (err, likedRemoved)=>{
                    if(err) return res.status(500).send({
                        message: 'Error en la peticion de likes'
                    });
    
                    if(!likedRemoved) return res.status(404).send({
                        message: 'No se ha encontrado referencia al likeded'
                    });
    
                    return res.status(200).send({
                        likedRemoved
                    });
                });
            }else{
                return res.status(200).send({
                    message: 'Este usuario no puede hacer dislike por otro usuario'
                });
            }
        });
}*/

function dislike(req,res){
    var movieId = req.params.id;
    var userId = req.user.sub

    Liked.findOne({movieLiked: movieId, user: userId}, (err, deleteLikes)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion del servidor'
        });

        if(!deleteLikes) return res.status(404).send({message: 'Imposible encontrar coincidencias'});

        Liked.findByIdAndRemove(deleteLikes._id, (err, deleteLikes)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion del servidor'
            });
    
            if(!deleteLikes) return res.status(404).send({message: 'Imposible encontrar coincidencias'});
            
            return res.status(200).send({
                deleteLikes
            });
        });
    });
}

function getNliked(req, res){
    var userId = req.params.id;
    var n = req.params.n;
    var swap;
    var cont;

    if(!userId){
        userId = req.user.sub;
    }
    
    if(req.params.type === 'serie'){
        swap = 'serieLiked';
    }else if(req.params.type === 'movie'){
        swap = 'movieLiked';
    }

    Liked.count({'user': userId}).exec((err, contador)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion al servidor'});
        cont = contador;
        if(n>contador){
            cont=n;
        }

        Liked.find({user: userId}).skip(cont-n).sort('likedAt').populate({path: swap}).exec((err, likeds)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion del servidor'
            });

            if(!likeds) return res.status(404).send({
                message: 'No hay concidencias con este contenido en el servidor'
            });
            if(likeds.season !== undefined){
                Season.findById(likeds.season).populate({path:'serie'}).exec((err, serieView) =>{
                    if(err) return res.status(500).send({
                        message: 'Error en la peticion del servidor'
                    });

                    if(!serieView || serieView === null || serieView === undefined) return res.status(404).send({
                        message: 'No se han encontrado series.'
                    });
                    console.log(serieView);
                    return res.status(200).send({
                        serieView,
                        contador
                    });
                });
            }

            return res.status(200).send({
                likeds,
                contador,
            });

        });
    });
}


module.exports= {
    likedContent,
    getLikeds,
    dislike,
    getNliked
}
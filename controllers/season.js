'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Serie = require('../models/serie');
var Season = require('../models/season');
var Chapter = require('../models/chapter');
var User = require('../models/user');
var Viewing = require('../models/viewing');


function newSeason(req, res){
    var params = req.body;
    var serieId = req.params.id;

    if(!serieId || !params.description) return res.status(200).send({
        message: 'No has cumplimentado los campos obligatorios'
    });
    
    Serie.findById(serieId, (err, serie)=>{
        if(err) res.status(500).send({
            message: 'Peticion erronea al servidor'
        });

        if(!serie) res.status(404).send({
            message: 'No ha sido posible encontrar la serie'
        });

        var season = new Season();

        season.serie = serieId;
        season.numSeason = params.numSeason;
        season.description = params.description;
        season.poster = params.file;

        season.save((err, seasonStored) =>{
            if(err) res.status(500).send({
                message: 'Error en la peticion al servidor'
            });

            if(!seasonStored) res.status(404).send({
                message: 'Error al guardar la pelicula en la base de datos'
            });

            return res.status(200).send({
            season: seasonStored
            });
        });
    });
}

function getSeason(req, res){
    var seasonId = req.params.id;

    Season.findById(seasonId, (err, season) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!season) return res.status(404).send({
            message: "Season inexistente"
        });

        return res.status(200).send({
            message: season
        });

        /*followThisSeason(req.season.sub, seasonId).then((value) => { 
            season.password = undefined;

            return res.status(200).send({
                season, 
                following: value.following, 
                followed: value.followed
            });
        });*/
    }).populate({path: 'serie'});
}

function getSeasons(req, res){
    var identitySeason =  req.params.id;

    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    Season.find().sort('_id').populate({path: 'serie'}).paginate(page, itemsPerPage, (err, seasons, total) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!seasons) return res.status(404).send({
            message: "No hay seasons disponibles"
        });

        return res.status(200).send({
            message: seasons
        });

        /*followSeasonsId(identitySeason).then((value) => {
            return res.status(200).send({
                seasons,
                following: value.followingSeasons,
                followed: value.followedSeasons,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        })
        .catch(err =>{
            console.log(err);
        });*/
    });
}

module.exports = {
    newSeason,
    getSeason,
    getSeasons
}
'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Movie = require('../models/movie');
var User = require('../models/user');
var Viewing = require('../models/viewing');
var Liked = require('../models/liked');

function newMovie(req, res){
    var params = req.body;

    if(!params.title || !params.description || !params.uri) return res.status(200).send({
        message: 'No has cumplimentado los campos obligatorios'
    });

    var movie = new Movie();

    movie.imdbID = params.imdbID;
    movie.title = params.title;
    movie.description = params.description;
    movie.genre = params.genre;
    movie.category = params.category;
    movie.year = params.year;
    movie.creator = params.creators;
    movie.duration = params.duration;
    movie.poster = params.file;
    movie.URI = params.uri;
    movie.uploadat = moment().unix();
    movie.userUp = req.user.sub;

    movie.save((err, movieStored) =>{
        if(err) res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!movieStored) res.status(404).send({
            message: 'Error al guardar la pelicula en la base de datos'
        });

        return res.status(200).send({
           movie: movieStored
        });
    });

}

function getMovie(req, res){
    var movieId = req.params.id;

    Movie.findById(movieId, (err, movie) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!movie) return res.status(404).send({
            message: "Pelicula inexistente"
        });

        return res.status(200).send({
            message: movie
        });

        /*followThisMovie(req.movie.sub, movieId).then((value) => { 
            movie.password = undefined;

            return res.status(200).send({
                movie, 
                following: value.following, 
                followed: value.followed
            });
        });*/
    });
}

function getMovieImdbID(req, res){
    var params = req.params;

    Movie.findOne({imdbID: params.imdbID}, (err, exist)=>{
        if(err) return res.status(500).send({message:'Error en la peticion al servidor'});

        if(!exist) return res.status(200).send({message: false});

        return res.status(200).send({exist});
    });
}

function getMovies(req, res){
    var identityMovie =  req.params.id;

    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Movie.find().sort('_id').paginate(page, itemsPerPage, (err, movies, total) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!movies) return res.status(404).send({
            message: "No hay peliculas disponibles"
        });

        return res.status(200).send({
            message: movies,
            total: total
        });

        /*followMoviesId(identityMovie).then((value) => {
            return res.status(200).send({
                movies,
                following: value.followingMovies,
                followed: value.followedMovies,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        })
        .catch(err =>{
            console.log(err);
        });*/
    });
}

//Actualizacion de datos del usuario
function updateMovie(req, res){

    var movieId = req.params.id;
    var update = req.body;

    //impedimos que el movie nos traiga contraseña
    delete update.password;

    Movie.findByIdAndUpdate(movieId, update, {new:true}, (err, movieUpdate) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });

        if(!movieUpdate) return res.status(404).send({
            message: 'No se ha encontrado usuario que actualizar'
        });

        return res.status(200).send({
            movie: movieUpdate
        });
    });
}

function getMoviesViewing(req, res){
    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }
    var itemsPerPage = 10;

    Viewing.find({user: req.user.sub, season: null}).populate('movieViewed').exec((err, viewing) => {
        if(err) return res.status(500).send({
            message: 'Error en la peticion'
        });
        var viewingClean = [];

        viewing.forEach((view)=>{
            viewingClean.push(view.movieViewed);
        });
        console.log(viewingClean);

    });
}

function getMovieTitle(req, res){
    var params = req.body;
    var page = 1;
    var itemsPerPage = 10;

    if(req.params.page){
        page = req.params.page; 
    }

    if(!params.title) return res.status(500).send({
        message: 'Barra de navegacion vacia'
    });

    Movie.find({title: params.title}).paginate(page, itemsPerPage,(err,movies, total)=>{
        if(err) return res.status(500).send({
            message: 'Error al hacer la peticion al servidor'
        });

        if(!movies) return res.status(404).send({
            message: 'No se han podido generar movies que coincidan'
        });

        return res.status(200).send({
            movies,
            total,
            pages: Math.ceil(total/itemsPerPage),
            page
        });
    });
}

function deleteMovie(req, res){
    var movieId = req.params.id;
    var delView;
    var delLiked;

    //Para poner en el boton sin necesidad de recarga, poner por post

    Movie.findById(movieId, (err, movie)=>{
        if(err) return res.status(500).send({
            message: 'La peticion al servidor ha sido incorrecta'
        });

        if(!movie) return res.status(404).send({
            message: 'Esta movie no se encuentra disponible en estos momentos'
        });

        if(movie.userUp == req.user.sub){
            
            Movie.findByIdAndRemove(movieId, (err, movieRemoved)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion movie'
                });

                if(!movieRemoved) return res.status(404).send({
                    message: 'No se ha encontrado referencia al movieed'
                });

                Viewing.find({movieViewed: movieId}).remove().exec((err, viewDel) => {
                    if(err) return res.status(500).send({
                        message: 'Error en la peticion al servidor'
                    });
                    delView = viewDel;
    
                });
    
                Liked.find({movieLiked: movieId}).remove().exec((err, viewLiked) => {
                    if(err) return res.status(500).send({
                        message: 'Error en la peticion al servidor'
                    });

                    delLiked = viewLiked;
                });

                return res.status(200).send({
                    movieRemoved,
                    delView,
                    delLiked
                });
            });

        }else{
            return res.status(200).send({
                message: 'Este usuario no puede borrar este contenido'
            });
        }
    });
}

//Subir avatar de usuario
function uploadPosterMovie(req, res){
    var movieId = req.params.id;

    if(req.files){
        var filePath = req.files.image.path; //Recojo el path de imagen
        var fileSplit = filePath.split('\/'); //Corto la ruta en una Array
        var fileName = fileSplit[3]; //Del Array anterior solo me quedo con el nombre del fichero
        var extSplit = fileName.split('\.'); //Cortamos el nombre del fichero en un Array
        var fileExt = extSplit[1]; //N os quedamos con la parte de la extension para verificar el tipo de fichero

        if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
            //Actualizamos poster
            Movie.findByIdAndUpdate(movieId, {poster: fileName}, {new:true}, (err, movieUpdate)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion'
                });
        
                if(!movieUpdate) return res.status(404).send({
                    message: 'No se ha encontrado usuario que actualizar'
                });
        
                return res.status(200).send({
                    movie: movieUpdate
                });
            });
        }else{
            return removeFilesUpload(res, filePath, 'Extension incorrecta');
        }

    }else{
        return res.status(200).send({
            message: 'No se han subido archivos'
        });
    }
}

function getMoviePoster(req, res){
    var imageFile = req.params.id;
    var pathFile = './uploads/movies/posters/'+imageFile;

    fs.exists(pathFile, (exists) => {
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            res.status(200).send({
                message: 'No existe la imagen'
            });
        }
    });
}

function removeFilesUpload(res, filePath, errorMessage){
    fs.unlink(filePath, (err) => {
        return res.status(200).send({
            message: errorMessage
        });
    });
}

function filterMovieGenre(req, res){
    var filterBy = req.params.genre;
    var filterByF= Number(req.params.yearf);
    var filterByL= Number(req.params.yearl);

    if(filterBy==='all'){
        filterBy='';
    }

    Movie.find({"genre": {$regex: filterBy,  $options: 'i'},"year": {"$gte": filterByF, "$lte": filterByL}}).exec((err, movie)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!movie) return res.status(404).send({
            message: 'No se han encontrado peliculas con este género'
        });

        return res.status(200).send({
            movie
        });
    });
}

function filterMovieYear(req, res){
    var filterBy= req.params.year;

    Movie.find({year: filterBy}, (err, movie)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!movie) return res.status(404).send({
            message: 'No se han encontrado peliculas con este año'
        });

        return res.status(200).send({
            movie
        });
    });
}

function filterSearch(req, res){
   var search = req.params.search;
   Movie.find({title: {$regex: search, $options: 'i'}}, (err,movies)=>{
       if(err) return res.status(500).send({
           message: 'Error en la peticion al servidor'
       });

       if(!movies) return res.status(404).send({
           message: 'No hemos encontrado coincidencias en el servidor'
       });

       return res.status(200).send({
           movies
       });

   });
}

module.exports = {
    newMovie,
    getMovie,
    getMovies,
    updateMovie,
    getMoviesViewing,
    getMovieTitle,
    deleteMovie,
    uploadPosterMovie,
    getMoviePoster,
    filterMovieGenre,
    filterMovieYear,
    filterSearch,
    getMovieImdbID
}
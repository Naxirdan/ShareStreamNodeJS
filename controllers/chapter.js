'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Serie = require('../models/serie');
var Season = require('../models/season');
var Chapter = require('../models/chapter');
var User = require('../models/user');
var Viewing = require('../models/viewing');

function newChapter(req, res){
    var params = req.body;
    var seasonId = req.params.id;

    if(!seasonId || !params.title || !params.numChapter || !params.description) return res.status(200).send({
        message: 'No has cumplimentado los campos obligatorios'
    });
    
    Season.findById(seasonId, (err, season)=>{
        if(err) res.status(500).send({
            message: 'Peticion erronea al servidor'
        });

        if(!season) res.status(404).send({
            message: 'No ha sido posible encontrar la serie'
        });

        var chapter = new Chapter();

        chapter.season = seasonId;
        chapter.numChapter = params.numChapter;
        chapter.title = params.title;
        chapter.description = params.description;
        chapter.poster = params.poster;
        chapter.duration = params.duration;
        chapter.uploadat = moment().unix();
        chapter.URI = params.uri;

        chapter.save((err, chapterStored) =>{
            if(err) res.status(500).send({
                message: 'Error en la peticion al servidor'
            });

            if(!chapterStored) res.status(404).send({
                message: 'Error al guardar la pelicula en la base de datos'
            });

            return res.status(200).send({
            chapter: chapterStored
            });
        });
    });
}

function getChapter(req, res){
    var chapterId = req.params.id;

    Chapter.findById(chapterId, (err, chapter) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!chapter) return res.status(404).send({
            message: "Capitulo inexistente"
        });

        return res.status(200).send({
            message: chapter
        });

        /*followThisChapter(req.chapter.sub, chapterId).then((value) => { 
            chapter.password = undefined;

            return res.status(200).send({
                chapter, 
                following: value.following, 
                followed: value.followed
            });
        });*/
    }).populate({path: 'season'});
}

function getChapters(req, res){
    var identityChapter =  req.params.id;

    var page = 1;
    
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    Chapter.find().sort('_id').populate({path: 'season'}).paginate(page, itemsPerPage, (err, chapters, total) => {
        if(err) return res.status(500).send({
            message: "Error en la peticion"
        });

        if(!chapters) return res.status(404).send({
            message: "No hay capitulo disponibles"
        });

        return res.status(200).send({
            message: chapters
        });

        /*followChaptersId(identityChapter).then((value) => {
            return res.status(200).send({
                chapters,
                following: value.followingChapters,
                followed: value.followedChapters,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        })
        .catch(err =>{
            console.log(err);
        });*/
    });
}


module.exports = {
    newChapter,
    getChapter,
    getChapters
}
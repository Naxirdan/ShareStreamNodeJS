'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Chapter = require('../models/chapter');
var Movie = require('../models/movie');
var Link = require('../models/contentlinks');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

function newLink(){
    var contentId = req.params.id;
    var tipo = req.params.tipo;
    var params = req.body;

    if(!params.uri) res.status(500).send({
        message: 'Debes introducir un url'
    });

    var content = new Link();
    content.userUp = req.user.sub;
    content.content = contentId;
    content.url = params.uri;
    content.uploadAt = moment().unix();

    content.save((err, linkStored)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!linkStored) return res.status(404).send({
            message: 'No se ha podido crear el enlace a este contenido'
        });

        return res.status(200).send({
            linkStored
        });
    });
}

function uploadVideo(req, res){
    var contentId = req.params.id;
    var tipo = req.params.tipo;

    if(req.files){
        var filePath = req.files.video.path; //Recojo el path del video
        var fileSplit = filePath.split('\/'); //Corto la ruta en una Array
        var fileName = fileSplit[2]; //Del Array anterior solo me quedo con el nombre del fichero
        var extSplit = fileName.split('\.'); //Cortamos el nombre del fichero en un Array
        var fileExt = extSplit[1]; //N os quedamos con la parte de la extension para verificar el tipo de fichero
        if(fileExt == 'mp4' || fileExt == 'avi'){
            //Actualizamos avatar
            /*Link.findOne({content: contentId}, {url: fileName}, {new:true}, (err, videoUpload)=>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion'
                });
        
                if(!videoUpload) return res.status(404).send({
                    message: 'No se ha encontrado contenido que actualizar'
                });*/

                var content = new Link();
                content.userUp = req.user.sub;
                content.url = fileName;
                content.uploadAt = moment().unix();
            
                if(tipo==='movie'){
                    content.contentMovie = req.params.id;

                    content.save((err, linkStored)=>{
                        if(err) return res.status(500).send({
                            message: 'Error en la peticion al servidor'
                        });
                
                        if(!linkStored) return res.status(404).send({
                            message: 'No se ha podido crear el enlace a este contenido'
                        });
                
                        return res.status(200).send({
                            linkStored
                        });
                    });  
                } else if(tipo==='serie'){
                    content.contentChapter = req.params.id;

                    content.save((err, linkStored)=>{
                        if(err) return res.status(500).send({
                            message: 'Error en la peticion al servidor'
                        });
                
                        if(!linkStored) return res.status(404).send({
                            message: 'No se ha podido crear el enlace a este contenido'
                        });
                
                        return res.status(200).send({
                            linkStored
                        });
                    });  
                }
                
                /*return res.status(200).send({
                    videoUpload
                });*/
            //});
        }else{
            return removeFilesUpload(res, filePath, 'Extension incorrecta');
        }

    }else{
        return res.status(200).send({
            message: 'No se han subido archivos'
        });
    }
}

function getVideoFile(req, res){
    var videoFile = req.params.videoFile;
    var pathFile = './uploads/videos/'+videoFile;

    fs.exists(pathFile, (exists) => {
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            res.status(200).send({
                message: 'No existe el contenido'
            });
        }
    });
}

function removeFilesUpload(res, filePath, errorMessage){
    fs.unlink(filePath, (err) => {
        return res.status(200).send({
            message: errorMessage
        });
    });
}

function getLinks(req, res){
    var contentId = req.params.idContent;

    Link.find({$or: [
        {contentMovie: contentId},
        {contentChapter: contentId}
        ]}).sort('uploadAt').exec((err, link)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion al servidor'
            });

            if(!link) return res.status(404).send({
                message: 'No se han encontrado links para este contenido'
            });

            return res.status(200).send({
                link
            });
        });
}

module.exports = {
    uploadVideo,
    getVideoFile,
    getLinks
}
'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Follow = require('../models/follow');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var Viewing = require('../models/viewing');
var ViewingShared = require('../models/viewingshared');
var Serie = require('../models/serie');
var Season = require('../models/season');
var Liked = require('../models/liked');
var moment = require('moment');

function viewingContent(req, res){
    var params = req.body;
    var typeCont = req.params.type;
    var contentId = req.params.id;
    var userId = req.user.sub;

    if(!typeCont || !contentId) {
        return res.status(404).send({message: 'Esta categoria no existe en la plataforma'});
    }

    var viewing = new Viewing();

    if(typeCont == 'movie'){

        if(!params.tiempo){
            viewing.timeViewed = '0';
        }else{
            viewing.timeViewed = req.params.tiempo;
        }

        viewing.user = req.user.sub;
        viewing.movieViewed = contentId;
        viewing.viewingAt = moment().unix();

        Viewing.find({movieViewed: contentId, user: userId}, (err, compView)=>{
            if(err) return res.status(500).send({
               message: 'Error en la peticion al server'
            });
            if(compView.length!==0){
                return res.status(200).send({
                    message: 'Ya estás viendo esta película'
                });
            }else{
                
                viewing.save((err, viewingTimeStored) =>{
                    console.log(viewingTimeStored);
                    if(err) res.status(500).send({
                        message: 'Error en la peticion al servidor'
                    });
            
                    if(!viewingTimeStored) res.status(404).send({
                        message: 'Error al guardar el tiempo de la pelicula en la base de datos'
                    });
            
                    return res.status(200).send({
                       movie: viewingTimeStored
                    });
                });
            }
        });
    
        /*viewing.save((err, viewingTimeStored) =>{
            if(err) res.status(500).send({
                message: 'Error en la peticion al servidor'
            });
    
            if(!viewingTimeStored) res.status(404).send({
                message: 'Error al guardar el tiempo de la pelicula en la base de datos'
            });
    
            return res.status(200).send({
               movie: viewingTimeStored
            });
        });*/

    }else if(typeCont == 'serie'){
        if(!params.tiempo) return res.status(200).send({
            message: 'Fallo al guardar la serie'
        });

        viewing.user = req.user.sub;
        viewing.serieViewed = contentId;
        viewing.season = params.contentId;
        viewing.chapter = params.chapter;
        viewing.timeViewed = req.params.tiempo;
        viewing.viewingAt = moment().unix();
    
        viewing.save((err, viewingTimeStored) =>{
            if(err) res.status(500).send({
                message: 'Error en la peticion al servidor'
            });
    
            if(!viewingTimeStored) res.status(404).send({
                message: 'Error al guardar el tiempo del capitulo en la base de datos'
            });
    
            return res.status(200).send({
               movie: viewingTimeStored
            });
        });
    }else{
        return res.status(404).send({
            message:'Esta categoria de contenido no existe'
        });
    }
}

function getViewing(req, res){
    var userId = req.params.id;
    var swap = null;
    if(!userId){
        userId = req.user.sub;
    }

    if(req.params.type === 'serie'){
        swap = 'chapter';
    }else if(req.params.type === 'movie'){
        swap = 'movieViewed';
    }

    Viewing.find({user: userId}).sort('viewingAt').populate('movieViewed').exec((err, view)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!view) return res.status(404).send({
            message: 'Este contenido no se ha encontrado en la lista de cola de este usuario'
        });

        return res.status(200).send({
            view
        });

    });
}

function getAllViewing(req, res){
    var userId = req.params.id;
    var page = 1;
    var itemsPerPage = 10;
    var swap;

    if(!userId){
        userId = req.user.sub;
    }
    
    if(!page){
        page = req.params.page;
    }

    if(req.params.type === 'serie'){
        swap = 'chapter';
    }else if(req.params.type === 'movie'){
        swap = 'movieViewed';
    }

    Viewing.find({user: userId}).sort('viewingAt').populate({path: swap}).paginate(page, itemsPerPage, (err, views, total)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion del servidor'
        });

        if(!views) return res.status(404).send({
            message: 'No hay concidencias con este contenido en el servidor'
        });
        if(views.season !== undefined){
            Season.findById(views.season).populate({path:'serie'}).exec((err, serieView) =>{
                if(err) return res.status(500).send({
                    message: 'Error en la peticion del servidor'
                });

                if(!serieView || serieView === null || serieView === undefined) return res.status(404).send({
                    message: 'No se han encontrado series.'
                });
                console.log(serieView);
                return res.status(200).send({
                    serieView,
                    page: page,
                    total: total,
                    pages: Math.ceil(total/itemsPerPage)
                });
            });
        }

        return res.status(200).send({
            views,
            page: page,
            total: total,
            pages: Math.ceil(total/itemsPerPage)
        });

    });

}

function getNview(req, res){
    var userId = req.params.id;
    var n = req.params.n;
    var swap;
    var cont;

    if(!userId){
        userId = req.user.sub;
    }
    
    if(req.params.type === 'serie'){
        swap = 'chapter';
    }else if(req.params.type === 'movie'){
        swap = 'movieViewed';
    }

    Viewing.count({'user': userId}).exec((err, contador)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion al servidor'});
        cont = contador;
        if(n>contador){
            cont=n;
        }

        Viewing.find({user: userId}).skip(cont-n).sort('viewingAt').populate({path: swap}).exec((err, views)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion del servidor'
            });

            if(!views) return res.status(404).send({
                message: 'No hay concidencias con este contenido en el servidor'
            });
            if(views.season !== undefined){
                Season.findById(views.season).populate({path:'serie'}).exec((err, serieView) =>{
                    if(err) return res.status(500).send({
                        message: 'Error en la peticion del servidor'
                    });

                    if(!serieView || serieView === null || serieView === undefined) return res.status(404).send({
                        message: 'No se han encontrado series.'
                    });
                    console.log(serieView);
                    return res.status(200).send({
                        serieView,
                        contador
                    });
                });
            }

            return res.status(200).send({
                views,
                contador
            });

        });
    });
}

function deleteViewing(req,res){
    var movieId = req.params.id;
    var userId = req.user.sub

    Viewing.findOne({movieViewed: movieId, user: userId}, (err, deleteViews)=>{
        if(err) return res.status(500).send({
            message: 'Error en la peticion del servidor'
        });

        if(!deleteViews) return res.status(404).send({message: 'Imposible encontrar coincidencias'});

        Viewing.findByIdAndRemove(deleteViews._id, (err, viewRemoved)=>{
            if(err) return res.status(500).send({
                message: 'Error en la peticion del servidor'
            });
    
            if(!deleteViews) return res.status(404).send({message: 'Imposible encontrar coincidencias'});
            
            return res.status(200).send({
                viewRemoved
            });
        });
    });
}

module.exports = {
    viewingContent,
    getViewing,
    getAllViewing,
    getNview,
    deleteViewing
}
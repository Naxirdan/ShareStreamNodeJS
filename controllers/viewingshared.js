'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Follow = require('../models/follow');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');
var Viewing = require('../models/viewing');
var ViewingShared = require('../models/viewingshared');
var Liked = require('../models/liked');
var moment = require('moment');

function viewingShared(req, res){
    var params = req.body;
    var userId = req.params.userId;
    var contentId = req.params.id;
    var timed = req.params.timed;

    if(!params.user) return res.status(200).send({
        message: 'No has cumplimentado los campos obligatorios'
    });

    var viewingShared = new ViewingShared();

    viewingShared.userOrder = userId;
    viewingShared.user = params.user;
    viewingShared.viewing = contentId;
    viewingShared.timeView = timed;
    viewingShared.sharedAt = moment().unix();


    viewingShared.save((err, viewingSharedStored) =>{
        if(err) res.status(500).send({
            message: 'Error en la peticion al servidor'
        });

        if(!viewingSharedStored) res.status(404).send({
            message: 'Error al guardar la pelicula en la base de datos'
        });

        return res.status(200).send({
           viewingShared: viewingSharedStored
        });
    });

}

module.exports = {
    viewingShared
}